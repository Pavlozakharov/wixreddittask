"use strict";

import HomeScreen from "./HomeScreen";
import FavouritesScreen from "./FavouritesScreen";
import React from "react";
import { Text, View, AsyncStorage } from "react-native";
import { createBottomTabNavigator } from "react-navigation";
import Icon from "react-native-vector-icons/Ionicons";

export default class MainNavigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: [], isLoading: true };
  }

  componentDidMount() {
    this._fetchData();
  }

  _executeQuery = query => {
    this.setState({ isLoading: true });
    fetch(query)
      .then(response => this._handleResponse(response))
      .catch(error => console.error(error));
  };

  _buildUrl = () => {
    const count = this.state.count;
    const after = this.state.after;
    const url = `https://www.reddit.com/r/popular.json?count=${count}&after=${after}`;
    return url;
  };

  _fetchData = () => {
    const url = this._buildUrl();
    this._executeQuery(url);
  };

  _handleResponse = response => {
    response.json().then(json => this._refreshData(json));
  };

  _reloadData = () => {
    this.setState({ shouldUpdate: true });
  };

  _refreshData(json) {
    const data = this.state.data;
    this.setState({
      isLoading: false,
      data: [...data, ...json.data.children],
      after: json.data.after
    });
  }

  render() {
    return (
      <TabBar
        screenProps={{
          _fetchData: this._fetchData,
          _reloadData: this._reloadData,
          isLoading: this.state.isLoading,
          data: this.state.data
        }}
      />
    );
  }
}

const TabBar = createBottomTabNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: ({ navigation }) => ({
        title: `Popular`,
        activeTintColor: "red",
        tabBarIcon: ({ focused, tintColor }) => {
          const iconName = `ios-home${focused ? "" : "-outline"}`;
          return <Icon name={iconName} size={30} color="red" />;
        }
      })
    },
    Favourites: {
      screen: FavouritesScreen,
      navigationOptions: ({ navigation }) => ({
        title: `Popular`,
        activeTintColor: "red",
        tabBarIcon: ({ focused, tintColor }) => {
          const iconName = `ios-heart${focused ? "" : "-outline"}`;
          return <Icon name={iconName} size={30} color="red" />;
        }
      })
    }
  },
  {
    tabBarOptions: {
      showLabel: false,
      activeTintColor: "#F8F8F8",
      inactiveTintColor: "#586589",
      style: {
        backgroundColor: "#3D3E41"
      },
      tabStyle: {}
    }
  }
);
