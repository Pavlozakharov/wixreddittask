"use strict";

import React, { Component } from "react";
import HomeScreen from "./HomeScreen";
import { Text, View } from "react-native";

export default class FavouritesScreen extends HomeScreen {
  _fetchData = () => {
    //To suppress fetching in Favourites component
  };

  _filterData = data => {
    const newData = data.filter(item => item.isFavourite);
    const filterText = this.state.filterText;
    if (!filterText || filterText == "") {
      return newData;
    }
    this.tableView.scrollToOffset({ y: 0, animated: false });
    return newData.filter(element =>
      element.data.title.toLowerCase().includes(filterText.toLowerCase())
    );
  };
}
