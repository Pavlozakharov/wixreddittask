"use strict";

import RedditPostListItem from "./RedditPostListItem";
import React, { Component } from "react";
import { SearchBar } from "react-native-elements";

import {
  StyleSheet,
  Image,
  View,
  TouchableHighlight,
  FlatList,
  Text,
  ActivityIndicator,
  ListItem,
  Button,
  WebView,
  Modal,
  AsyncStorage
} from "react-native";

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: props.screenProps.isLoading,
      isWebViewLoading: true,
      data: [],
      count: 0,
      after: null,
      modalVisible: false,
      shouldLoadMore: true
    };
  }

  itemClicked = item => {
    this._showModal(item);
  };

  _showModal(item) {
    this.setState({
      modalVisible: true,
      selectedItem: item,
      selectedUrl: item.data.url,
      isSelectedItemFavourite: item.isFavourite
    });
  }

  _keyExtractor = (item, index) => index;

  _renderItem = ({ item, index }) => (
    <RedditPostListItem
      itemClicked={this.itemClicked}
      item={item}
      index={index}
    />
  );

  _handleMoreRequests = () => {
    if (this.state.shouldLoadMore) {
      this.setState({
        count: (this.state.count += 25)
      });
      this.props.screenProps._fetchData();
    }
  };

  _filterData = data => {
    const filterText = this.state.filterText;
    if (!filterText || filterText == "" || filterText.length < 3) {
      return data;
    }
    this.tableView.scrollToOffset({ y: 0, animated: false });
    return data.filter(element =>
      element.data.title.toLowerCase().includes(filterText.toLowerCase())
    );
  };

  _onCancelButtonPressed = () => {
    this.setState({ modalVisible: false, isWebViewLoading: true });
  };

  _onLikeButtonPressed = selectedItem => {
    if (selectedItem.isFavourite) {
      selectedItem.isFavourite = false;
    } else {
      selectedItem.isFavourite = true;
    }
    this.setState({ modalVisible: false, isWebViewLoading: true });
    this.props.screenProps._reloadData();
  };

  _onSearchBarTextChanged = text => {
    if (text.length < 3) {
      this.setState({ filterText: text, shouldLoadMore: true });
    } else {
      this.setState({
        filterText: text,
        shouldLoadMore: false,
        isLoading: false
      });
    }
  };

  render() {
    const spinner = this.state.isLoading ? (
      <ActivityIndicator size="large" style={styles.activityIndicator} />
    ) : null;
    const webViewSpinner = this.state.isWebViewLoading ? (
      <ActivityIndicator size="large" style={styles.activityIndicatorWebView} />
    ) : null;
    const selectedItem = this.state.selectedItem;
    const titleForLikeButton = this.state.isSelectedItemFavourite ? "💔" : "❤️";
    return (
      <View style={styles.container}>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
        >
          <View style={styles.columnContainerModal}>
            <View style={styles.rowContainerModal}>
              <Button
                onPress={() => this._onCancelButtonPressed()}
                title="Cancel"
                color="red"
              />
              <Button
                onPress={() => this._onLikeButtonPressed(selectedItem)}
                title={titleForLikeButton}
              />
            </View>
            <WebView
              source={{ uri: this.state.selectedUrl }}
              onLoadEnd={() => this.setState({ isWebViewLoading: false })}
            />
            {webViewSpinner}
          </View>
        </Modal>
        <SearchBar
          ref={search => (this.search = search)}
          clearIcon={{ color: "red" }}
          onChangeText={text => this._onSearchBarTextChanged(text)}
          placeholder="Type Here..."
          value={this.state.filterText}
        />
        <FlatList
          ref={tableView => (this.tableView = tableView)}
          style={styles.flatList}
          data={this._filterData(this.props.screenProps.data)}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
          onEndReached={this._handleMoreRequests}
          onEndThreshold={1}
        />
        {spinner}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  activityIndicator: {
    marginTop: 20
  },
  activityIndicatorWebView: {
    position: "absolute",
    top: "50%",
    left: "50%",
    marginLeft: -18,
    marginTop: -18
  },
  description: {
    marginBottom: 20,
    fontSize: 18,
    textAlign: "center",
    color: "#656565"
  },
  container: {
    padding: 0,
    marginTop: 35
  },
  flowRight: {
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "stretch"
  },
  searchInput: {
    height: 36,
    padding: 4,
    marginRight: 5,
    flexGrow: 1,
    fontSize: 18,
    borderWidth: 1,
    borderColor: "#48BBEC",
    borderRadius: 8,
    color: "#48BBEC"
  },
  flatList: {
    flex: 0
  },
  image: {
    width: 217,
    height: 138
  },
  rowContainer: {
    flexDirection: "row",
    padding: 10
  },
  rowContainerModal: {
    flexDirection: "row",
    padding: 20,
    justifyContent: "space-between"
  },
  columnContainerModal: {
    flexDirection: "column",
    marginTop: 15,
    flex: 1
  }
});
