"use strict";

import React from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableHighlight,
  Image,
  Modal,
  Button,
  WebView
} from "react-native";
import { createBottomTabNavigator } from "react-navigation";

export default class RedditPostListItem extends React.PureComponent {
  _buildImageView = url => {
    var imageView;
    if (url == "nsfw") {
      imageView = (
        <Image source={require("../Resources/nsfw.jpg")} style={styles.thumb} />
      );
    } else if (!url.startsWith("https")) {
      imageView = (
        <Image
          source={require("../Resources/reddit.jpg")}
          style={styles.thumb}
        />
      );
    } else {
      imageView = <Image style={styles.thumb} source={{ uri: url }} />;
    }

    return imageView;
  };

  render() {
    const item = this.props.item;
    const name = item.data.title;
    const imageUrl = item.data.thumbnail;
    const url = item.data.url;

    return (
      <TouchableHighlight
        onPress={() => this.props.itemClicked(item)}
        underlayColor="#dddddd"
      >
        <View>
          <View style={styles.rowContainer}>
            {this._buildImageView(imageUrl)}
            <View style={styles.textContainer}>
              <Text
                style={styles.title}
                numberOfLines={3}
                ellipsizeMode={"tail"}
              >
                {name}
              </Text>
            </View>
          </View>
          <View style={styles.separator} />
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  thumb: {
    width: 60,
    height: 60,
    marginRight: 10
  },
  separator: {
    height: 1,
    backgroundColor: "#dddddd"
  },
  textContainer: {
    flex: 1,
    flexWrap: "wrap"
  },
  title: {
    fontSize: 18,
    color: "#656565"
  },
  rowContainer: {
    flexDirection: "row",
    padding: 10
  },
  rowContainerModal: {
    flexDirection: "row",
    padding: 20,
    justifyContent: "space-between"
  },
  columnContainerModal: {
    flexDirection: "column",
    flex: 1
  }
});
